class Question < ApplicationRecord
  has_many :alternatives
  has_many :match_questions
  has_many :matches, through: :match_questions
  scope :random_questions, -> (limit) { limit(limit).order('RANDOM()')}
  scope :by_level, -> (level) { where(level: level) }
  belongs_to :trail
  enum level: { Baby: 0, Teen: 1, Master: 2}
  accepts_nested_attributes_for :alternatives
end
