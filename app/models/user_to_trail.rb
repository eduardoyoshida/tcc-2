class UserToTrail < ApplicationRecord
  belongs_to :user
  belongs_to :trail
  has_many :questions, through: :trail
  enum level: { Baby: 0, Teen: 1, Master: 2}, _default: 0
  scope :order_by_correct, -> { order('correct_amount DESC') }
  scope :by_trail, -> (trail_id) { where(trail_id: trail_id) }

  LEVEL_QUESTIONS = {
    Baby: 100,
    Teen: 300
  }

  def get_question
    self.questions.by_level(self.level).random_questions(1)
  end

  def add_correct
    self.correct_amount += 1
    self.level = UserToTrail.levels[self.level] + 1 if check_level
    self.save
  end

  def add_wrong
    self.wrong_amount += 1
    self.save
  end

  def check_level
    self.correct_amount == LEVEL_QUESTIONS[self.level.to_sym]
  end
end
