class Alternative < ApplicationRecord
  belongs_to :question
  enum letter: { A: 0, B: 1, C: 2, D: 3, E: 4 }

  scope :correct_alternative, -> { find_by(correct: true )}
end
