class Badge < ApplicationRecord
  has_many :users, through: :user_to_badge

  LIST = {
    gold: 0,
    silver: 1,
    bronze: 2,
    certificate: 3,
    trail: 4
  }
end
