class Answer < ApplicationRecord
  belongs_to :user
  belongs_to :match, optional: true
  belongs_to :question
  belongs_to :user_to_trail, optional: true
  belongs_to :alternative, optional: true
  before_create :check_correct

  after_create :update_match, if: :is_match?
  after_create :update_user_trail, if: :is_trail?

  scope :by_user, -> (user) { where(user: user) }
  scope :in_match, -> (match) { where(match: match) }
  scope :correct_answers, -> { where(correct: true) }
  scope :wrong_answers, -> { where(correct: false) }
  scope :with_questions, -> { joins(:question) }
  scope :valid, -> { where(timedout: false) }
  scope :from_matches, -> { where("match_id is not null")}
  scope :from_trail, -> { where("user_to_trail_id is not null")}

  def check_correct
    self.correct = self.alternative.correct
  end

  def self.calculate_correct(user, match)
    self.correct_answers.by_user(user).in_match(match).valid.count
  end

  private

  def update_match
    self.match.update_player_question(self.user)
  end

  def update_user_trail
    if self.correct
      self.user_to_trail.add_correct
    else
      self.user_to_trail.add_wrong
    end
  end

  def is_match?
    !self.match_id.nil?
  end

  def is_trail?
    !self.user_to_trail_id.nil?
  end

  def warn
    ActionCable.server.broadcast("Match-#{self.match_id}", "test")
  end
end
