class Match < ApplicationRecord
  belongs_to :player1, class_name: 'User'
  belongs_to :player2, class_name: 'User', optional: true
  belongs_to :winner, class_name: 'User', optional: true
  belongs_to :loser, class_name: 'User', optional: true
  has_many :match_questions
  has_many :questions, through: :match_questions
  before_create :set_match_info
  before_create :set_match_questions

  enum status: { awaiting: 0, ongoing: 1, finished: 2 }

  enum player1_status: { started: 0, done: 1}, _prefix: true
  enum player2_status: { started: 0, done: 1}, _prefix: true
  
  scope :with_questions, -> { joins(:questions) }

  def get_question(current_user)
    if current_user == self.player1
      self.questions.order(:id)[self.player1_current_question]
    else
      self.questions.order(:id)[self.player2_current_question]
    end
  end

  def update_player_question(current_user)
    num_questions = self.questions.length - 1
    if current_user == self.player1 && self.player1_status_started?
      if self.player1_current_question >= num_questions
        self.player1_status = :done
        self.save
        return
      end
      self.player1_current_question += 1
    elsif current_user == self.player2 && self.player2_status_started?
      if self.player2_current_question >= num_questions
        self.player2_status = :done
        self.save
        return
      end
      self.player2_current_question += 1
    end
    self.save
  end

  def update_match_status
    if self.player1_status_done? && self.player2_status_done?
      self.status = :finished
      self.calculate_winner
    end
  end
  private

  def set_match_info
    self.status = :awaiting
    self.player1_current_question = 0
    self.player2_current_question = 0
  end

  def set_match_questions
    self.questions = Question.random_questions(5)
  end

  def calculate_winner
    correct_by_player1 = Answer.calculate_correct(self.player1, self.id)
    correct_by_player2 = Answer.calculate_correct(self.player2, self.id)
    winner = correct_by_player1 > correct_by_player2 ? self.player1 : self.player2
    self.winner = winner
    self.loser = self.player1 == self.winner ? self.player2 : self.player1
    self.status = :finished
    self.update_scoreboard
    self.save
  end

  def update_scoreboard
    current_position = ChallengeScoreboard.get_position(self.winner.id)
    self.winner.challenge_scoreboard.add_win
    self.loser.challenge_scoreboard.add_loss
    new_position = ChallengeScoreboard.get_position(self.winner.id)
    self.update_badges(current_position, new_position)
  end

  def update_badges(current_position, new_position)
    if (new_position < current_position && new_position < 3)
      UserToBadge.create(user_id: self.winner.id, badge_id: new_position)
    end
  end
end
