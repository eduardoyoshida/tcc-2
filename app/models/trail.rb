class Trail < ApplicationRecord
  has_many :user_to_trails
  has_many :users, through: :user_to_trails
  has_many :questions
end

