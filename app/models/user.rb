class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable
  has_many :answers
  has_many :user_to_trail
  has_many :user_to_badge
  has_many :trails, through: :user_to_trail
  has_many :badges, through: :user_to_badge
  has_one :challenge_scoreboard
  after_create :create_scoreboard
  after_create :create_trails

  def can_play?
    self.challenge_scoreboard.points >= 25
  end

  def update_last_login
    self.last_login = Time.now
    self.save
  end

  def verify_reward_available
    self.update_last_login if self.last_login.nil?
    available = (Time.now - self.last_login) > 24.hours
    if available
      self.update_last_login
      self.challenge_scoreboard.add_bonus
    end
    available
  end

  private

  def create_scoreboard
    ChallengeScoreboard.create(user: self, points: 100)
  end

  def create_trails
    trails = Trail.all
    trails.each do |trail|
      UserToTrail.create(user: self, trail: trail)
    end
  end
end
