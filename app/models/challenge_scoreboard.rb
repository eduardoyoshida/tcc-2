class ChallengeScoreboard < ApplicationRecord
  belongs_to :user

  default_scope { order('points DESC') }

  scope :slice_ranking, ->  (offset) { offset(offset).limit(10) }

  def add_win
    self.wins += 1
    self.points += 25
    self.save
  end
  
  def add_loss
    self.losses += 1
    self.points -= 25
    self.save
  end

  def add_bonus
    self.points += 100
    self.save
  end

  def self.get_position(user_id)
    self.pluck(:id).index(user_id)
  end

  def self.rank_position_offset(user_id)
    position = self.get_position(user_id)
    [position - 5, 0 ].max
  end

  def self.rank_slice(position)
    self.slice_ranking(position)
  end

end
