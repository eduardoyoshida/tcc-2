json.extract! challenge_scoreboard, :id, :user_id, :wins, :losses, :points, :created_at, :updated_at
json.url challenge_scoreboard_url(challenge_scoreboard, format: :json)
