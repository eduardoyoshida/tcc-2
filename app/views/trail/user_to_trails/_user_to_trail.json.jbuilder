json.extract! user_to_trail, :id, :user_id, :trail_id, :level, :correct_amount, :wrong_amount, :created_at, :updated_at
json.url user_to_trail_url(user_to_trail, format: :json)
