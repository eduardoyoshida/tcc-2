json.extract! answer, :id, :value, :correct, :user_id, :match_id, :question_id, :created_at, :updated_at
json.url answer_url(answer, format: :json)
