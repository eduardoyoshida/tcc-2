class UsersController < ApplicationController
  before_action :set_user, only: :show

  def index
    @users = User.all
  end

  def show
    if !@user.nil?
      @rank_position = ChallengeScoreboard.rank_position_offset(@user.id)
      @rank_slice = ChallengeScoreboard.rank_slice(@rank_position)
      @rank_position += 1
    end
  end

  private

  def set_user
    @user = User.find(params[:id])
  end
end
