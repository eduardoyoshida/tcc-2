class Trail::AnswersController < ApplicationController
  before_action :set_answer, only: %i[ show ]
  before_action :validate_user, only: %i[ create ]

  def index
    @answers = Answer.all
  end

  def show
  end

  def new
    @answer = Answer.new
  end

  def create
    level = UserToTrail.find(answer_params[:user_to_trail_id]).level
    @answer = Answer.new(answer_params.merge(user_id: current_user.id))

    respond_to do |format|
      if @answer.save!
        new_level = UserToTrail.find(answer_params[:user_to_trail_id]).level
        flash[:notice] = "Parabéns!!!! Você passou de nível!!!" if level != new_level
        format.html { redirect_to controller: "user_to_trails", action: "resolution", id: @answer.user_to_trail.id, answer_id: @answer.id }
        format.json { render :show, status: :created, location: @answer }
        format.js
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @answer.errors, status: :unprocessable_entity }
        format.js
      end
    end
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_answer
    @answer = Answer.find(params[:id])
  end

  def answer_params
    params.require(:answer).permit(:value, :alternative_id, :question_id, :timedout, :user_to_trail_id)
  end

  def validate_user
    trail = UserToTrail.find(params[:id])
    unless current_user == trail.user
      flash[:error] = "Falha ao responder pergunta, você não está vinculado a essa trilha"
    end
  end
end
