class Trail::TrailScoreboardsController < ApplicationController
  before_action :set_trail, only: [:show]
  before_action :set_users, only: [:show]

  def index
    @trails = Trail.all
  end

  def show
  end

  private

  def set_trail
    @trail = Trail.find(params[:id])
  end

  def set_users
    @rank_slice  = UserToTrail.by_trail(params[:id]).order_by_correct
  end
end
