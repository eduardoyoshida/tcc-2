class Trail::UserToTrailsController < ApplicationController
  before_action :set_user_to_trail, only: %i[ show play resolution ]

  # GET /user_to_trails or /user_to_trails.json
  def index
    @user_to_trails = UserToTrail.where(user_id: current_user.id)
  end

  # GET /user_to_trails/1 or /user_to_trails/1.json
  def show
  end

  def play
    @question = @user_to_trail.get_question.first
  end

  def resolution
    @answer = Answer.find(params[:answer_id])
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def check_user_allowed
      unless @user_to_trail.user == current_user
        redirect_to root_path
      end
    end

    def set_user_to_trail
      @user_to_trail = UserToTrail.find(params[:id])
      check_user_allowed
    end

    # Only allow a list of trusted parameters through.
    def user_to_trail_params
      params.require(:user_to_trail).permit(:user_id, :trail_id, :level, :correct_amount, :wrong_amount)
    end
end
