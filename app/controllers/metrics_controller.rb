class MetricsController < ApplicationController
  def index
    @correct = Answer.by_user(current_user).correct_answers.group_by_day(:created_at).count
    @wrong = Answer.by_user(current_user).wrong_answers.group_by_day(:created_at).count
    @total =Answer.by_user(current_user).group_by_day(:created_at).count
  end
end
