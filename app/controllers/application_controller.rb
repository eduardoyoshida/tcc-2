class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception

  before_action :configure_permitted_parameters, if: :devise_controller?
  before_action :authenticate_user!
  before_action :load_trail
  before_action :verify_last_login

  protected

    def configure_permitted_parameters
      devise_parameter_sanitizer.permit(:sign_up) { |u| u.permit(:name, :email, :password, :password_confirmation)}

      devise_parameter_sanitizer.permit(:account_update) { |u| u.permit(:name, :email, :password, :current_password)}
    end

    def after_sign_in_path_for(resource)
      user_path(current_user) # your path
    end

    def load_trail
      @trails = Trail.all
    end

    def verify_last_login
      flash[:notice] = "Parabéns por sua assiduidade! Como recompensa você recebeu 100 pontos no modo desafio!" if current_user && current_user.verify_reward_available
    end
end
