class Match::AnswersController < ApplicationController
  before_action :set_answer, only: %i[ show ]
  before_action :validate_user, only: %i[ create ]
  after_action :set_match_status, only: %i[ create ]

  def index
    @answers = Answer.all
  end

  def show
  end

  def new
    @answer = Answer.new
  end

  def create
    @answer = Answer.new(answer_params.merge(user_id: current_user.id))

    respond_to do |format|
      if @answer.save!
        format.html { redirect_to controller: "matches", action: "question", id: @answer.match.id }
        format.json { render :show, status: :created, location: @answer }
        format.js
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @answer.errors, status: :unprocessable_entity }
        format.js
      end
    end
  end

  private
  def set_answer
    @answer = Answer.find(params[:id])
  end

  def set_match_status
    match = Match.find(answer_params[:match_id])
    match.update_match_status
  end

  def answer_params
    params.require(:answer).permit(:value, :match_id, :alternative_id, :question_id, :timedout)
  end

  def validate_user
    match = Match.find(answer_params[:match_id])
    unless current_user == match.player1 || current_user == match.player2
      flash[:error] = "Falha ao responder pergunta, você não está vinculado a essa partida"
    end
  end
end
