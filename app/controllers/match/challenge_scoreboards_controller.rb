class Match::ChallengeScoreboardsController < ApplicationController
  before_action :set_challenge_scoreboard, only: %i[ show ]

  def index
    @rank_slice = ChallengeScoreboard.all
  end

  def show
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_challenge_scoreboard
      @challenge_scoreboard = ChallengeScoreboard.rank_slice(0)
    end

    # Only allow a list of trusted parameters through.
    def challenge_scoreboard_params
      params.require(:challenge_scoreboard).permit(:user_id, :wins, :losses, :points)
    end
end
