class Match::ReportsController < ApplicationController
  before_action :set_match, only: %i[ show ]
  before_action :set_match_answers, only: %i[ show ]

  def show
    flash[:notice] = "Parabéns #{current_user.name} por vencer a partida!!! Você recebeu 25 pontos no modo desafio. Veja sua posição no ranking #{helpers.link_to("aqui", match_challenge_scoreboards_path)}" if current_user == @match.winner
    flash[:notice] = "Poxa! Você perdeu a partida e, portanto, 25 pontos do modo desafio , mas não desanime #{current_user.name}, reveja seus erros e continue aprendendo!" if current_user == @match.loser
  end

  private

  def set_match
    @match = Match.find(params[:id])
  end

  def set_match_answers
    @player1_answers = fetch_player_answers(@match.player1)
    @player2_answers = fetch_player_answers(@match.player2)
    @player1_answer_count = fetch_correct_amount(@match.player1)
    @player2_answer_count = fetch_correct_amount(@match.player2)
  end

  def fetch_correct_amount(user)
    Answer.correct_answers.by_user(user).in_match(@match).valid.count
  end

  def fetch_player_answers(user)
    Answer.by_user(user).with_questions.in_match(@match)
  end
end
