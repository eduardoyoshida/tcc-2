class Match::MatchesController < ApplicationController
  before_action :set_match, only: %i[ show edit join destroy question ]
  before_action :can_play, only: %i[ create join ]
  # GET /matches or /matches.json
  def index
    @matches = Match.where('player1_id = ? or player2_id = ?', current_user.id, current_user.id)
  end

  # GET /matches/1 or /matches/1.json
  def show
    redirect_to report_match_match_path(@match) if @match.finished?
    @player1_answer_count = Answer.correct_answers.by_user(@match.player1).in_match(@match).valid.count if !@match.player1.nil?
    @player2_answer_count = Answer.correct_answers.by_user(@match.player2).in_match(@match).valid.count if !@match.player2.nil?
  end

  # GET /matches/new
  def new
    @match = Match.new
  end

  # GET /matches/1/edit
  def edit
  end

  # POST /matches or /matches.json
  def create
    @match = Match.new(match_params.merge(player1: current_user))

    respond_to do |format|
      if @match.save!
        format.html { redirect_to match_match_path(@match), notice: "Partida criada com sucesso. Envie este link para o usuário que deseja desafiar http://localhost:3000#{match_match_path(@match)}" }
        format.json { render :show, status: :created, location: @match }
      else
        flash[:notice] = "Pontuação insuficiente para iniciar uma partida"
        format.html { render :index, status: :unprocessable_entity }
        format.json { render json: @match.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /matches/1 or /matches/1.json
  def join
    respond_to do |format|
      if @match.update(player2: current_user, status: :ongoing)
        format.html { redirect_to match_match_path(@match), notice: "Você entrou na partida." }
        format.json { render :show, status: :ok, location: @match }
        warn_join({
          type: "player_join",
          message: "O jogador #{@match.player2.name} entrou na partida",
          player: @match.player2.name
        })
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @match.errors, status: :unprocessable_entity }
      end
    end
  end

  def question
    redirect_to match_match_path(@match.id) if @match.player1 == current_user && @match.player1_status_done?
    redirect_to match_match_path(@match.id) if @match.player2 == current_user && @match.player2_status_done?

    @question = @match.get_question(current_user)
  end

  # DELETE /matches/1 or /matches/1.json
  def destroy
    @match.destroy
    respond_to do |format|
      format.html { redirect_to matches_url, notice: "Partida criada com sucesso." }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_match
      @match = Match.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def match_params
      params.permit()
    end

    def warn_join(message)
      ActionCable.server.broadcast("Match-#{@match.id}", message)
    end

    def can_play
      unless current_user.can_play?
        puts "Foi aqui???"
        flash[:error] = "Devido a sua pontuação, você não está habilitado a entrar em partidas PVP!"
        redirect_to match_matches_path
      end
    end
end
