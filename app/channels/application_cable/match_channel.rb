class MatchChannel < ApplicationCable::Channel
  def subscribed
    if params[:match_id].present?
      stream_from("Match-#{params[:match_id]}")
    end
  end

  def subscribed
    current_user.appear
  end

  def unsubscribed
    current_user.disappear
  end

  def appear(data)
    current_user.appear(on: data['appearing_on'])
  end

  def away
    current_user.away
  end

  # def answer(data)
  #   question = get_question(data["question"])
  #   user = get_user(data["user"])
  #   question_answer = data["answer"]
  #   match = get_match(data["match"])
  #   Answer.create!(
  #     user: user,
  #     match: match,
  #     question: question,
  #     value: question_answer,
  #     correct: question_answer == question.correct_answer
  #   )
  # end

  # private 

  # def get_user(id)
  #   User.find_by(id)
  # end

  # def get_question(id)
  #   Question.find_by(id)
  # end

  # def get_match(id)
  #   Match.find_by(id)
  # end
end
