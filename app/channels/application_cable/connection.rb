module ApplicationCable
  class Connection < ActionCable::Connection::Base
    rescue_from StandardError, with: :log_error
    identified_by :current_user

    def connect
      self.current_user = find_verified_user
    end

    private

    def find_verified_user
      if verified_user = env['warden'].user
        verified_user
      end
    end

    def log_error(e)
      puts(e)
    end
  end
end
