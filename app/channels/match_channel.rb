class MatchChannel < ApplicationCable::Channel
  def subscribed
    puts params
    if params[:match_id].present?
      stream_from("Match-#{params[:match_id]}")
    end
  end

  def unsubscribed
    # Any cleanup needed when channel is unsubscribed
  end

  def update_question(data)
    ActionCable.server.broadcast "Match-#{params[:match_id]}", message: render_question
  end

  private

  def render_question
    ApplicationController.renderer.render(partial: 'questions/show', locals: { question: Question.second } )
  end
end