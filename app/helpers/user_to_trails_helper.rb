module UserToTrailsHelper
  def progress_bar_percentage(user_to_trail)
    goal_amount = UserToTrail::LEVEL_QUESTIONS[user_to_trail.level.to_sym]
    "#{(user_to_trail.correct_amount.to_f / goal_amount.to_f) * 100}% "
  end
end
