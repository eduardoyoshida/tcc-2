module MatchesHelper
  def match_status(status)
    statuses = {
      ongoing: "A partida está pronta para começar",
      awaiting: "Aguardando o jogador 2 entrar na partida",
      finished: "Partida encerrada"
    }
    statuses[status.to_sym]
    end
end
