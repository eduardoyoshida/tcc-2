module ApplicationHelper
  def flash_class(level)
    table = {
      notice: "alert alert-info alert-dismissible",
      success: "alert alert-success alert-dismissible",
      error: "alert alert-error alert-dismissible",
      alert: "alert alert-error alert-dismissible"
    }
    table[level.to_sym]
  end
end
