import introJs from "intro.js";

window.addEventListener("DOMContentLoaded", (event) => {
  document.getElementById("help").addEventListener("click", function (e) {
    e.preventDefault();
    var card = document.getElementsByClassName("card mb-4")[0];
    introJs().setOptions({
      steps: [
        {
          element: card,
          title: 'Esta é a questão a ser respondida!',
          intro: 'As questões são obitdas a partir das bases de dados do ENEM.'
        },
        {
          element: card.querySelector('.row'),
          title: 'De onde vem a questão?',
          intro: 'O cabeçalho detalha em qual prova e caderno esta questão foi aplicada.\n Nas partidas do modo desafio, para ser contabilizada, a questão deve ser respondida no tempo delimitado.'
        },
        {
          element: document.getElementsByClassName('answer_form')[0],
          title: 'Selecione uma alternativa e clique em responder!',
          intro: 'Nas partidas do modo desafio, você poderá ver as respostas das questões ao final do desafio. No modo trilha, assim que responder você poderá visualizar a resposta correta.'
        }
      ]
    }).start()
  })
});
