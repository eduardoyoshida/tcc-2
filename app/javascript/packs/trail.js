import introJs from "intro.js";

window.addEventListener("DOMContentLoaded", (event) => {
  document.getElementById("help").addEventListener("click", function (e) {
    e.preventDefault();
    var card = document.getElementsByClassName("card mb-4")[0];
    introJs().setOptions({
      steps: [
        {
          element: card,
          title: 'Este cartão se refere a uma trilha',
          intro: 'Nele você pode visualizar seus erros e acertos, bem como seu progresso em níveis na trilha'
        },
        {
          element: card.querySelector('.progress'),
          title: 'Barra de progresso',
          intro: 'Esta barra demonstra seu progresso entre os níveis na trilha.\n Os níveis são: Baby, Teen, Master.'
        },
        {
          element: card.querySelector('.btn'),
          title: 'Jogar!!!',
          intro: 'Para começar a evoluir basta clicar em jogar e responder as perguntas desta trilha!!'
        }
      ]
    }).start()
  })
});
