import introJs from "intro.js";
import { countdown } from "./utils/timer";

window.addEventListener("DOMContentLoaded", (event) => {
  document.getElementById("help").addEventListener("click", function (e) {
    e.preventDefault();
    var card = document.getElementsByClassName("card mb-4")[0];
    introJs().setOptions({
      steps: [
        {
          element: card,
          title: 'Questão!',
          intro: 'Aqui está descrita a questão da trilha que você escolheu, conforme seu nível na trilha!'
        },
        {
          element: document.getElementById("current_level"),
          title: 'Seu nível atual!',
          intro: 'Aqui você pode visualizar seu nível atual!'
        },
        {
          element: document.getElementsByClassName("progress")[0],
          title: 'Seu progresso até o próximo nível',
          intro: 'Aqui você pode visualizar seu progresso até o próximo nível! Ao passar de nível você tem acesso a questões mais difíceis e pode desbloquear recompensas!'
        },
        {
          element: document.getElementById('notebook'),
          title: 'Caderno da Questão',
          intro: 'Este é o caderno de onde foi extraída essa questão.'
        },
        {
          element: document.getElementById('timer'),
          title: 'Cronômetro',
          intro: 'Esse é o tempo sugerido para resoluçaõ dessa questão, tal como você teria no ENEM.'
        },
        {
          element: document.getElementsByClassName('form-check')[0],
          title: 'Selecione uma alternativa e clique em responder!',
          intro: 'Nas partidas do modo desafio, você poderá ver as respostas das questões ao final do desafio. No modo trilha, assim que responder você poderá visualizar a resposta correta.'
        },
      ]
    }).start()
  });
  countdown();
});
