export function countdown() {
  if (!document.getElementById("timer")) return;
  var currentTime = new Date();
  var countDownDate = currentTime.setMinutes(currentTime.getMinutes() + 3)
  countDownDate = new Date(countDownDate).getTime();

  // Update the count down every 1 second
  var x = setInterval(function() {

    // Get today's date and time
    var now = new Date().getTime();

    // Find the distance between now and the count down date
    var distance = countDownDate - now;

    // Time calculations for days, hours, minutes and seconds
    var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
    var seconds = Math.floor((distance % (1000 * 60)) / 1000);

    // Display the result in the element with id="demo"
    document.getElementById("timer").innerHTML = minutes + "m" + seconds + "s ";

    // If the count down is finished, write some text
    if (distance < 0) {
      clearInterval(x);
      document.getElementById("timer").innerHTML = "0m 0s";
      document.getElementById("answer_timedout").value = true;
      alert('O tempo para resposta dessa questão expirou, mas não se preocupe, você ainda pode respondê-la, porém ela não será contabilizada no resultado deste desafio.')
    }
  }, 1000);
}
