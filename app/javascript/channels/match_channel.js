import consumer from "./consumer"
import { player_join } from "./resources/match"

export function createSubscription(matchId) {
  consumer.subscriptions.create({ channel: "MatchChannel", match_id: matchId }, {
    // Called once when the subscription is created.
    initialized() {
      this.update = this.update.bind(this)
    },
  
    // Called when the subscription is ready for use on the server.
    connected() {
      this.install()
      this.update()
      console.log("Subscribed to match", matchId)
    },
  
    // Called when the WebSocket connection is closed.
    disconnected() {
      this.uninstall()
    },
  
    // Called when the subscription is rejected by the server.
    rejected() {
      this.uninstall()
    },
  
    update() {
      console.log('update')
    },
  
    appear() {
      // Calls `AppearanceChannel#appear(data)` on the server.
      this.perform("update_question")
    },
  
    away() {
      // Calls `AppearanceChannel#away` on the server.
      this.perform("away")
    },
  
    received(payload) {
      if (payload.type == "player_join") {
        player_join(payload)
      }
    },
  
    install() {
      window.addEventListener("focus", this.update)
      window.addEventListener("blur", this.update)
      document.addEventListener("turbolinks:load", this.update)
      document.addEventListener("visibilitychange", this.update)
    },
  
    uninstall() {
      window.removeEventListener("focus", this.update)
      window.removeEventListener("blur", this.update)
      document.removeEventListener("turbolinks:load", this.update)
      document.removeEventListener("visibilitychange", this.update)
    },
  
    get documentIsActive() {
      return document.visibilityState == "visible" && document.hasFocus()
    },
  
    get appearingOn() {
      const element = document.querySelector("[data-appearing-on]")
      return element ? element.getAttribute("data-appearing-on") : null
    }
  })
}
