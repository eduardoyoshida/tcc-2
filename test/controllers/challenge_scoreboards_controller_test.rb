require "test_helper"

class ChallengeScoreboardsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @challenge_scoreboard = challenge_scoreboards(:one)
  end

  test "should get index" do
    get challenge_scoreboards_url
    assert_response :success
  end

  test "should get new" do
    get new_challenge_scoreboard_url
    assert_response :success
  end

  test "should create challenge_scoreboard" do
    assert_difference('ChallengeScoreboard.count') do
      post challenge_scoreboards_url, params: { challenge_scoreboard: { losses: @challenge_scoreboard.losses, points: @challenge_scoreboard.points, user_id: @challenge_scoreboard.user_id, wins: @challenge_scoreboard.wins } }
    end

    assert_redirected_to challenge_scoreboard_url(ChallengeScoreboard.last)
  end

  test "should show challenge_scoreboard" do
    get challenge_scoreboard_url(@challenge_scoreboard)
    assert_response :success
  end

  test "should get edit" do
    get edit_challenge_scoreboard_url(@challenge_scoreboard)
    assert_response :success
  end

  test "should update challenge_scoreboard" do
    patch challenge_scoreboard_url(@challenge_scoreboard), params: { challenge_scoreboard: { losses: @challenge_scoreboard.losses, points: @challenge_scoreboard.points, user_id: @challenge_scoreboard.user_id, wins: @challenge_scoreboard.wins } }
    assert_redirected_to challenge_scoreboard_url(@challenge_scoreboard)
  end

  test "should destroy challenge_scoreboard" do
    assert_difference('ChallengeScoreboard.count', -1) do
      delete challenge_scoreboard_url(@challenge_scoreboard)
    end

    assert_redirected_to challenge_scoreboards_url
  end
end
