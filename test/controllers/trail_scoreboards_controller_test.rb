require "test_helper"

class TrailScoreboardsControllerTest < ActionDispatch::IntegrationTest
  test "should get index" do
    get trail_scoreboards_index_url
    assert_response :success
  end

  test "should get show" do
    get trail_scoreboards_show_url
    assert_response :success
  end
end
