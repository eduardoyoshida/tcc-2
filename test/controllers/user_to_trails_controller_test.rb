require "test_helper"

class UserToTrailsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @user_to_trail = user_to_trails(:one)
  end

  test "should get index" do
    get user_to_trails_url
    assert_response :success
  end

  test "should get new" do
    get new_user_to_trail_url
    assert_response :success
  end

  test "should create user_to_trail" do
    assert_difference('UserToTrail.count') do
      post user_to_trails_url, params: { user_to_trail: { correct_amount: @user_to_trail.correct_amount, level: @user_to_trail.level, trail_id: @user_to_trail.trail_id, user_id: @user_to_trail.user_id, wrong_amount: @user_to_trail.wrong_amount } }
    end

    assert_redirected_to user_to_trail_url(UserToTrail.last)
  end

  test "should show user_to_trail" do
    get user_to_trail_url(@user_to_trail)
    assert_response :success
  end

  test "should get edit" do
    get edit_user_to_trail_url(@user_to_trail)
    assert_response :success
  end

  test "should update user_to_trail" do
    patch user_to_trail_url(@user_to_trail), params: { user_to_trail: { correct_amount: @user_to_trail.correct_amount, level: @user_to_trail.level, trail_id: @user_to_trail.trail_id, user_id: @user_to_trail.user_id, wrong_amount: @user_to_trail.wrong_amount } }
    assert_redirected_to user_to_trail_url(@user_to_trail)
  end

  test "should destroy user_to_trail" do
    assert_difference('UserToTrail.count', -1) do
      delete user_to_trail_url(@user_to_trail)
    end

    assert_redirected_to user_to_trails_url
  end
end
