require "application_system_test_case"

class ChallengeScoreboardsTest < ApplicationSystemTestCase
  setup do
    @challenge_scoreboard = challenge_scoreboards(:one)
  end

  test "visiting the index" do
    visit challenge_scoreboards_url
    assert_selector "h1", text: "Challenge Scoreboards"
  end

  test "creating a Challenge scoreboard" do
    visit challenge_scoreboards_url
    click_on "New Challenge Scoreboard"

    fill_in "Losses", with: @challenge_scoreboard.losses
    fill_in "Points", with: @challenge_scoreboard.points
    fill_in "User", with: @challenge_scoreboard.user_id
    fill_in "Wins", with: @challenge_scoreboard.wins
    click_on "Create Challenge scoreboard"

    assert_text "Challenge scoreboard was successfully created"
    click_on "Back"
  end

  test "updating a Challenge scoreboard" do
    visit challenge_scoreboards_url
    click_on "Edit", match: :first

    fill_in "Losses", with: @challenge_scoreboard.losses
    fill_in "Points", with: @challenge_scoreboard.points
    fill_in "User", with: @challenge_scoreboard.user_id
    fill_in "Wins", with: @challenge_scoreboard.wins
    click_on "Update Challenge scoreboard"

    assert_text "Challenge scoreboard was successfully updated"
    click_on "Back"
  end

  test "destroying a Challenge scoreboard" do
    visit challenge_scoreboards_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Challenge scoreboard was successfully destroyed"
  end
end
