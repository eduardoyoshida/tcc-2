require "application_system_test_case"

class UserToTrailsTest < ApplicationSystemTestCase
  setup do
    @user_to_trail = user_to_trails(:one)
  end

  test "visiting the index" do
    visit user_to_trails_url
    assert_selector "h1", text: "User To Trails"
  end

  test "creating a User to trail" do
    visit user_to_trails_url
    click_on "New User To Trail"

    fill_in "Correct amount", with: @user_to_trail.correct_amount
    fill_in "Level", with: @user_to_trail.level
    fill_in "Trail", with: @user_to_trail.trail_id
    fill_in "User", with: @user_to_trail.user_id
    fill_in "Wrong amount", with: @user_to_trail.wrong_amount
    click_on "Create User to trail"

    assert_text "User to trail was successfully created"
    click_on "Back"
  end

  test "updating a User to trail" do
    visit user_to_trails_url
    click_on "Edit", match: :first

    fill_in "Correct amount", with: @user_to_trail.correct_amount
    fill_in "Level", with: @user_to_trail.level
    fill_in "Trail", with: @user_to_trail.trail_id
    fill_in "User", with: @user_to_trail.user_id
    fill_in "Wrong amount", with: @user_to_trail.wrong_amount
    click_on "Update User to trail"

    assert_text "User to trail was successfully updated"
    click_on "Back"
  end

  test "destroying a User to trail" do
    visit user_to_trails_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "User to trail was successfully destroyed"
  end
end
