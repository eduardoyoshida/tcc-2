class AddAlternativeToAnswer < ActiveRecord::Migration[6.1]
  def change
    add_reference :answers, :alternative, null: true
  end
end
