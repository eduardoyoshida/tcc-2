class RenameAnswerColumn < ActiveRecord::Migration[6.1]
  def change
    rename_column :answers, :value, :choice
  end
end
