class ChangeAlternativesLetter < ActiveRecord::Migration[6.1]
  def change
    change_column :alternatives, :letter, 'integer USING CAST(letter AS integer)'
  end
end
