class AddPlayerCurrentQuestionToMatch < ActiveRecord::Migration[6.1]
  def change
    add_column :matches, :player1_current_question, :integer
    add_column :matches, :player2_current_question, :integer
  end
end
