class CreateUserToTrails < ActiveRecord::Migration[6.1]
  def change
    create_table :user_to_trails do |t|
      t.references :user, null: false, foreign_key: true
      t.references :trail, null: false, foreign_key: true
      t.integer :level
      t.integer :correct_amount, default: 0
      t.integer :wrong_amount, default: 0

      t.timestamps
    end
  end
end
