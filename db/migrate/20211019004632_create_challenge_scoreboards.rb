class CreateChallengeScoreboards < ActiveRecord::Migration[6.1]
  def change
    create_table :challenge_scoreboards do |t|
      t.references :user, null: false, foreign_key: true
      t.integer :wins, default: 0
      t.integer :losses, default: 0
      t.integer :points, default: 0

      t.timestamps
    end
  end
end
