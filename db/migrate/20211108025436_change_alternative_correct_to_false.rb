class ChangeAlternativeCorrectToFalse < ActiveRecord::Migration[6.1]
  def change
    change_column_default :alternatives, :correct, false
  end
end
