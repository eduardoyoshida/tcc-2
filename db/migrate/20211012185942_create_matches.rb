class CreateMatches < ActiveRecord::Migration[6.1]
  def change
    create_table :matches do |t|
      t.references :player1, null: true, foreign_key: { to_table: :users }
      t.references :player2, null: true, foreign_key: { to_table: :users }
      t.references :winner, null: true, foreign_key: { to_table: :users }
      t.integer :status

      t.timestamps
    end
  end
end
