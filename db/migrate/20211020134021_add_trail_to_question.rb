class AddTrailToQuestion < ActiveRecord::Migration[6.1]
  def change
    add_reference :questions, :trail
  end
end
