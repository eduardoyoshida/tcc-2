class AddTimeoutToAnswer < ActiveRecord::Migration[6.1]
  def change
    add_column :answers, :timedout, :boolean, default: true
  end
end
