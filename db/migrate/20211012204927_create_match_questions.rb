class CreateMatchQuestions < ActiveRecord::Migration[6.1]
  def change
    create_table :match_questions do |t|
      t.references :match, null: false, foreign_key: true
      t.references :question, null: false, foreign_key: true

      t.timestamps
    end
  end
end
