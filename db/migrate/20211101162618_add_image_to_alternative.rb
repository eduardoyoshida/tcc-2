class AddImageToAlternative < ActiveRecord::Migration[6.1]
  def change
    add_column :alternatives, :image, :string
  end
end
