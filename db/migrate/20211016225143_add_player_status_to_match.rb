class AddPlayerStatusToMatch < ActiveRecord::Migration[6.1]
  def change
    add_column :matches, :player1_status, :integer, default: 0
    #Ex:- :default =>''
    add_column :matches, :player2_status, :integer, default: 0
  end
end
