class AddTrailToAnswer < ActiveRecord::Migration[6.1]
  def change
    add_reference :answers, :user_to_trail, null: true
  end
end
