class AddResolutionToQuestion < ActiveRecord::Migration[6.1]
  def change
    add_column :questions, :resolution, :text
  end
end
