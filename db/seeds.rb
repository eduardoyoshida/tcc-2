# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the bin/rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
Dir[File.join(Rails.root, 'db', 'seeds', '*.rb')].sort.each do |seed|
  load seed
end

# 50.times do |index|
#   q = Question.create({title: "Questão #{index} de Bio", body: "Descrição da Questão #{index}", trail: bio, correct_answer: 'C', level: rand(0..2)})
#   Alternative.create({text: "Alternativa A da questão #{index}", letter: 'A', correct: false, question: q })
#   Alternative.create({text: "Alternativa B da questão #{index}", letter: 'B', correct: false, question: q })
#   Alternative.create({text: "Alternativa C da questão #{index}", letter: 'C', correct: true, question: q })
#   Alternative.create({text: "Alternativa D da questão #{index}", letter: 'D', correct: false, question: q })
#   Alternative.create({text: "Alternativa E da questão #{index}", letter: 'E', correct: false, question: q })
# end

# 50.times do |index| 
#   q = Question.create({title: "Questão #{index} de Fis", body: "Descrição da Questão #{index}", trail: fis, correct_answer: 'D', level: rand(0..2)})
#   Alternative.create({text: "Alternativa A da questão #{index}", letter: 'A', correct: false, question: q })
#   Alternative.create({text: "Alternativa B da questão #{index}", letter: 'B', correct: false, question: q })
#   Alternative.create({text: "Alternativa C da questão #{index}", letter: 'C', correct: false, question: q })
#   Alternative.create({text: "Alternativa D da questão #{index}", letter: 'D', correct: true, question: q })
#   Alternative.create({text: "Alternativa E da questão #{index}", letter: 'E', correct: false, question: q })
# end

# 50.times do |index|
#   q = Question.create({title: "Questão #{index} de Mat", body: "Descrição da Questão #{index}", trail: mat, correct_answer: 'B', level: rand(0..2)})
#   Alternative.create({text: "Alternativa A da questão #{index}", letter: 'A', correct: false, question: q })
#   Alternative.create({text: "Alternativa B da questão #{index}", letter: 'B', correct: true, question: q })
#   Alternative.create({text: "Alternativa C da questão #{index}", letter: 'C', correct: false, question: q })
#   Alternative.create({text: "Alternativa D da questão #{index}", letter: 'D', correct: false, question: q })
#   Alternative.create({text: "Alternativa E da questão #{index}", letter: 'E', correct: false, question: q })
# end

badges = [
  {image: 'badges/gold.png', title: 'Primeiro Lugar em Desafios', description: nil },
  {image: 'badges/silver.png', title: 'Segundo Lugar em Desafios', description: nil },
  {image: 'badges/bronze.png', title: 'Terceiro Lugar em Desafios', description: nil },
  {image: 'badges/certificate.png', title: 'Primeira Vitória', description: nil },
  {image: 'badges/trophy.png', title: 'Primeiro Lugar em Trilha', description: nil }
]

badges.each do |badge|
  Badge.create(badge)
end