bio = Trail.create({name: 'Biologia', description: 'Ciências biológicas'})
mat = Trail.create({name: 'Matemática', description: 'Matemática e suas tecnologias'})
fis = Trail.create({name: 'Física', description: 'Física'})

physics = [
  {
    trail: fis,
    from: 'QUESTÃO 48 – Caderno ROSA – ENEM 2016',
    body: 'Uma ambulância A em movimento retilíneo e uniforme aproxima-se de um observador O, em
    repouso. A sirene emite um som de frequência constante fa. O desenho ilustra as frentes de
    onda do som emitido pela ambulância. O observador possui um detector que consegue
    registrar, no esboço de um gráfico, a frequência da onda sonora detectada em função do
    tempo f0 (t), antes e depois da passagem da ambulância por ele.',
    resolution: 'Nesta questão o conteúdo cobrado é a interpretação gráfica do efeito doppler. O efeito doppler é a
    diferença da frequência das ondas causada por uma aproximação ou afastamento da fonte sonora
    (ou observador). À medida que a ambulância se aproxima do observador, há um aumento de
    frequência, ficando maior que a frequência da fonte. Ao se afastar, observa-se uma diminuição da
    frequência, ficando, assim, menor que a frequência da fonte.',
    correct_answer: 3,
    image: 'fisica/questions/1.png',
    alternatives_attributes: [
      { text: nil, letter: 0, image: 'fisica/answers/1-a.png'},
      { text: nil, letter: 1, image: 'fisica/answers/1-b.png'},
      { text: nil, letter: 2, image: 'fisica/answers/1-c.png'},
      { text: nil, letter: 3, image: 'fisica/answers/1-d.png', correct: true},
      { text: nil, letter: 4, image: 'fisica/answers/1-e.png'}
    ]
  },
  {
    trail: fis,
    from: 'QUESTÃO 52 – CADERNO ROSA – ENEM 2016',
    body: 'A figura 1 apresenta o gráfico da intensidade, em decibéis (dB), da onda sonora emitida por um alto-
    falante, que está em repouso, e medida por um microfone em função da frequência da onda para
    diferentes distâncias: 3 mm, 25 mm, 51 mm e 60 mm. A Figura 2 apresenta um diagrama com a
    indicação das diversas faixas do espectro de frequência sonora para o modelo de alto-falante
    utilizado neste experimento.',
    title: 'Relacionando as informações presentes nas figuras 1 e 2, como a intensidade sonora percebida é
    afetada pelo aumento da distância do microfone ao alto-falante?',
    resolution: 'Observando o gráfico é possível perceber que à medida que se aumenta a distância do microfone na
    região do grave, a intensidade diminui.',
    correct_answer: 2,
    image: 'fisica/questions/2.png',
    alternatives_attributes: [
      { text: 'Aumenta na faixa das frequências médias.', letter: 0},
      { text: 'Diminui na faixa das frequências agudas.', letter: 1},
      { text: 'Diminui na faixa das frequências graves.', letter: 2, correct: true},
      { text: 'Aumenta na faixa das frequências médias altas.', letter: 3},
      { text: 'Aumenta na faixa das frequências médias baixas.', letter: 4}
    ]
  },
  {
    trail: fis,
from: 'QUESTÃO 54 – CADERNO ROSA – ENEM 2016',
    body: 'O morcego emite pulsos de curta duração de ondas ultrassônicas, os quais voltam na forma de ecos
    após atingirem objetos no ambiente, trazendo informações a respeito das suas dimensões, suas
    localizações e dos seus possíveis movimentos. Isso se dá em razão da sensibilidade do morcego em
    detectar o tempo gasto para os ecos voltarem, bem como das pequenas variações nas frequências e
    nas intensidades dos pulsos ultrassônicos. Essas características lhe permitem caçar pequenas presas
    mesmo quando estão em movimento em relação a si. Considere uma situação unidimensional em
    que uma mariposa se afasta, em movimento retilíneo e uniforme, de um morcego em repouso.',
    title: 'A distância e velocidade da mariposa, na situação descrita, seriam detectadas pelo sistema de um
    morcego por quais alterações nas características dos pulsos ultrassônicos?',
    resolution: 'A intensidade sonora da emissão pela borboleta é dada por:
    <br>
    A onda sonora é uma onda tridimensional. Logo, a área de propagação é a área da esfera:
    <br>
    O que significa dizer que à medida que a mariposa se distância, a intensidade sonora diminui com o
    quadrado da distância e o tempo de retorno aumenta. Pelo conceito de efeito doppler, à medida que
    a fonte se afasta do observador, a frequência diminui. Basta pensar no exemplo de uma ambulância:
    quando ela se aproxima do observador o som se torna mais agudo, ou seja, a frequência é maior. Ao
    se afastar, o som se torna mais grave e a frequência, então, é menor.',
    correct_answer: 0,
    alternatives_attributes: [
      { text: 'Intensidade diminuída, o tempo de retorno aumentado e a frequência percebida diminuída.', letter: 0, correct: true},
      { text: 'Intensidade aumentada, o tempo de retorno diminuído e a frequência percebida diminuída.', letter: 1},
      { text: 'Intensidade diminuída, o tempo de retorno diminuído e a frequência percebida aumentada.', letter: 2},
      { text: 'Intensidade diminuída, o tempo de retorno aumentado e a frequência percebida aumentada.', letter: 3},
      { text: 'Intensidade aumentada, o tempo de retorno aumentado e a frequência percebida aumentada.', letter: 4}
    ]
  },
  {
    trail: fis,
    from: 'QUESTÃO 58 – CADERNO ROSA – ENEM 2016',
    body: 'A invenção e o acoplamento entre engrenagens revolucionaram a ciência na época e propiciaram a
    invenção de várias tecnologias, como os relógios. Ao construir um pequeno cronômetro, um
    relojoeiro usa o sistema de engrenagens mostrado. De acordo com a figura, um motor é ligado ao
    eixo e movimenta as engrenagens fazendo o ponteiro girar. A frequência do motor é de 18 RPM, e o
    número de dentes das engrenagens está apresentado no quadro.',
    title: 'A frequência de giro do ponteiro, em RPM, é',
    resolution: 'A intensidade sonora da emissão pela borboleta é dada por:
    <br>
    A onda sonora é uma onda tridimensional. Logo, a área de propagação é a área da esfera:
    <br>
    O que significa dizer que à medida que a mariposa se distância, a intensidade sonora diminui com o
    quadrado da distância e o tempo de retorno aumenta. Pelo conceito de efeito doppler, à medida que
    a fonte se afasta do observador, a frequência diminui. Basta pensar no exemplo de uma ambulância:
    quando ela se aproxima do observador o som se torna mais agudo, ou seja, a frequência é maior. Ao
    se afastar, o som se torna mais grave e a frequência, então, é menor.',
    correct_answer: 2,
    image: 'fisica/questions/4.png',
    alternatives_attributes: [
      { text: '1.', letter: 0},
      { text: '2.', letter: 1},
      { text: '4.', letter: 2, correct: true},
      { text: '81.', letter: 3},
      { text: '162.', letter: 4}
    ]
  },
  {
    trail: fis,
    from: 'QUESTÃO 67 – CADERNO ROSA – ENEM 2016',
    body: 'A magnetohipertermia é um procedimento terapêutico que se baseia na elevação da temperatura
    das células de uma região específica do corpo que estejam afetadas por um tumor. Nesse tipo de
    tratamento, nanopartículas magnéticas são fagocitadas pelas células tumorais, e um campo
    magnético alternado externo é utilizado para promover a agitação das nanopartículas e consequente
    aquecimento da célula.',
    title: 'A elevação de temperatura descrita ocorre porque',
    resolution: 'Quando submetida a um campo magnético variável, a nanopartícula adquire um torque,
    transferindo calor por atrito.',
    correct_answer: 1,
    alternatives_attributes: [
      { text: 'o campo magnético gerado pela oscilação das nanopartículas é absorvido pelo tumor.', letter: 0},
      { text: 'o campo magnético alternado faz as nanopartículas girarem, transferindo calor por atrito.', letter: 1, correct: true},
      { text: 'as nanopartículas interagem magneticamente com as células do corpo, transferindo calor.', letter: 2},
      { text: 'o campo magnético alternado fornece calor para as nanopartículas que o transfere às células do corpo.', letter: 3},
      { text: 'as nanopartículas são aceleradas em um único sentido em razão da interação com o campo magnético, fazendo-as colidir com as células e transferir calor.', letter: 4}
    ]
  },
  {
    trail: fis,
    from: 'QUESTÃO 74 – CADERNO ROSA – ENEM 2016',
    body: 'A magnetohipertermia é um procedimento terapêutico que se baseia na elevação da temperatura
    das células de uma região específica do corpo que estejam afetadas por um tumor. Nesse tipo de
    tratamento, nanopartículas magnéticas são fagocitadas pelas células tumorais, e um campo
    magnético alternado externo é utilizado para promover a agitação das nanopartículas e consequente
    aquecimento da célula.',
    title: 'Considerando a situação descrita, qual esboço gráfico representa a velocidade do automóvel
em relação à distância percorrida até parar totalmente?',
    resolution: 'A partir do momento que o motorista percebe que deverá fazer uma frenagem brusca, o movimento
    se torna um movimento acelerado, ou seja, um MRUV. Entre a percepção do motorista e o
    acionamento do freio há um instante chamado de tempo de reação. Nesse período o automóvel
    permanece com mesma velocidade. Logo em seguida há o acionamento do freio. Neste instante o
    movimento pode ser expresso pela equação de Torricelli . Perceba que a relação que existe entre
    velocidade e distância é do tipo quadrática. Como é uma, o gráfico VxD assume a forma de uma
    parábola com concavidade para baixo e, nos primeiros metros, a velocidade tende a permanecer
    constante.',
    correct_answer: 3,
    alternatives_attributes: [
      { text: 'o campo magnético gerado pela oscilação das nanopartículas é absorvido pelo tumor.', letter: 0, image: 'fisica/answers/6-a.png'},
      { text: 'o campo magnético alternado faz as nanopartículas girarem, transferindo calor por atrito.', letter: 1, image: 'fisica/answers/6-b.png'},
      { text: 'as nanopartículas interagem magneticamente com as células do corpo, transferindo calor.', letter: 2, image: 'fisica/answers/6-c.png'},
      { text: 'o campo magnético alternado fornece calor para as nanopartículas que o transfere às células do corpo.', letter: 3, image: 'fisica/answers/6-d.png', correct: true},
      { text: 'as nanopartículas são aceleradas em um único sentido em razão da interação com o campo magnético, fazendo-as colidir com as células e transferir calor.', letter: 4, image: 'fisica/answers/6-e.png'},
    ]
  },
  {
    trail: fis,
    from: 'QUESTÃO 80 – CADERNO ROSA – ENEM 2016',
    body: 'Um experimento para comprovar a natureza ondulatória da radiação de micro-ondas foi realizado da
    seguinte forma: anotou-se a frequência de operação de um forno de micro-ondas e, em seguida,
    retirou-se sua plataforma giratória. No seu lugar, colocou-se uma travessa refratária com uma
    camada grossa de manteiga. Depois disso, o forno foi ligado por alguns segundos. Ao se retirar a
    travessa refratária do forno, observou-se que havia três pontos de manteiga derretida alinhados
    sobre toda a travessa. Parte da onda estacionária gerada no interior do forno é ilustrada na figura.',
    title: 'De acordo com a figura, que posições correspondem a dois pontos consecutivos da manteiga
    derretida?',
    resolution: 'Existe dois tipos de interferência: a CONSTRUTIVA e a DESTRUTIVA. A interferência que resulta
    num ponto máximo e é chamada de INTERFERÊNCIA CONSTRUTIVA. Já quando a interferência
    resulta num ponto nulo, essa é uma INTERFERÊNCIA DESTRUTIVA. No caso da manteiga no
    micro-ondas, os pontos que derretem são onde há a maior intensidade das micro-ondas, ou
    seja, nos pontos de vales e cristas. A única opção possível seguindo esse raciocínio é a que
    contém os pontos I e III, pois a questão solicita que sejam pontos CONSECUTIVOS.',
    image: 'fisica/questions/7.png',
    correct_answer: 0,
    alternatives_attributes: [
      { text: 'I e III', letter: 0, correct: true},
      { text: 'I e V', letter: 1},
      { text: 'II e III', letter: 2},
      { text: 'II e IV', letter: 3},
      { text: 'II e V', letter: 4},
    ]
  },
]

physics.each do |question|
  Question.create(question)
end