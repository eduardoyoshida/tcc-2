# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `bin/rails
# db:schema:load`. When creating a new database, `bin/rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2021_11_08_025436) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "alternatives", force: :cascade do |t|
    t.string "text"
    t.integer "letter"
    t.boolean "correct", default: false
    t.bigint "question_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "image"
    t.index ["question_id"], name: "index_alternatives_on_question_id"
  end

  create_table "answers", force: :cascade do |t|
    t.integer "choice"
    t.boolean "correct"
    t.bigint "user_id", null: false
    t.bigint "match_id"
    t.bigint "question_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.boolean "timedout", default: true
    t.bigint "user_to_trail_id"
    t.bigint "alternative_id"
    t.index ["alternative_id"], name: "index_answers_on_alternative_id"
    t.index ["match_id"], name: "index_answers_on_match_id"
    t.index ["question_id"], name: "index_answers_on_question_id"
    t.index ["user_id"], name: "index_answers_on_user_id"
    t.index ["user_to_trail_id"], name: "index_answers_on_user_to_trail_id"
  end

  create_table "badges", force: :cascade do |t|
    t.string "image"
    t.string "title"
    t.string "description"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "challenge_scoreboards", force: :cascade do |t|
    t.bigint "user_id", null: false
    t.integer "wins", default: 0
    t.integer "losses", default: 0
    t.integer "points", default: 0
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["user_id"], name: "index_challenge_scoreboards_on_user_id"
  end

  create_table "match_questions", force: :cascade do |t|
    t.bigint "match_id", null: false
    t.bigint "question_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["match_id"], name: "index_match_questions_on_match_id"
    t.index ["question_id"], name: "index_match_questions_on_question_id"
  end

  create_table "matches", force: :cascade do |t|
    t.bigint "player1_id"
    t.bigint "player2_id"
    t.bigint "winner_id"
    t.integer "status"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.integer "player1_current_question"
    t.integer "player2_current_question"
    t.integer "player1_status", default: 0
    t.integer "player2_status", default: 0
    t.bigint "loser_id"
    t.index ["loser_id"], name: "index_matches_on_loser_id"
    t.index ["player1_id"], name: "index_matches_on_player1_id"
    t.index ["player2_id"], name: "index_matches_on_player2_id"
    t.index ["winner_id"], name: "index_matches_on_winner_id"
  end

  create_table "questions", force: :cascade do |t|
    t.string "title"
    t.text "body"
    t.integer "correct_answer"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.bigint "trail_id"
    t.integer "level", default: 0
    t.string "image"
    t.text "resolution"
    t.string "from"
    t.index ["trail_id"], name: "index_questions_on_trail_id"
  end

  create_table "trails", force: :cascade do |t|
    t.string "name"
    t.text "description"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "user_to_badges", force: :cascade do |t|
    t.bigint "user_id", null: false
    t.bigint "badge_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["badge_id"], name: "index_user_to_badges_on_badge_id"
    t.index ["user_id"], name: "index_user_to_badges_on_user_id"
  end

  create_table "user_to_trails", force: :cascade do |t|
    t.bigint "user_id", null: false
    t.bigint "trail_id", null: false
    t.integer "level"
    t.integer "correct_amount", default: 0
    t.integer "wrong_amount", default: 0
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["trail_id"], name: "index_user_to_trails_on_trail_id"
    t.index ["user_id"], name: "index_user_to_trails_on_user_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "name"
    t.date "birthdate"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.datetime "last_login", default: -> { "CURRENT_TIMESTAMP" }
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

  add_foreign_key "alternatives", "questions"
  add_foreign_key "answers", "matches"
  add_foreign_key "answers", "questions"
  add_foreign_key "answers", "users"
  add_foreign_key "challenge_scoreboards", "users"
  add_foreign_key "match_questions", "matches"
  add_foreign_key "match_questions", "questions"
  add_foreign_key "matches", "users", column: "loser_id"
  add_foreign_key "matches", "users", column: "player1_id"
  add_foreign_key "matches", "users", column: "player2_id"
  add_foreign_key "matches", "users", column: "winner_id"
  add_foreign_key "user_to_badges", "badges"
  add_foreign_key "user_to_badges", "users"
  add_foreign_key "user_to_trails", "trails"
  add_foreign_key "user_to_trails", "users"
end
