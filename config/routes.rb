Rails.application.routes.draw do
  get 'metrics/index'
  get '/', to: redirect('landing.html')
  resources :users, only: [:index, :show], path: '/player'
  namespace :trail do
    resources :trail_scoreboards, only: [:index, :show], path: '/ranking'
    resources :user_to_trails, only: [:index, :show], path: '/' do
      member do
        get 'play', action: :play
        get 'resolution/:answer_id', action: :resolution
        resources :answers, only: [:create]
      end
    end
  end

  resources :metrics, only: [:index]

  namespace :match do
    resources :challenge_scoreboards, only: :index, path: '/ranking'
    resources :matches, path: '/' do
      member do
        put 'join'
        get 'question'
        get 'report', action: :show, controller: 'reports'
        resources :answers, only: [:create]
      end
    end
  end
  mount ActionCable.server => '/cable'

  resources :questions
  resources :matches do
    member do
      get 'question'
    end
  end

  devise_for :users, controllers: {
    sessions: 'users/sessions',
    registrations: 'users/registrations'
  }
end
